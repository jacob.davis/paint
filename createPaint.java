/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paintprogram;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionListener;
import java.io.File;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import static java.awt.event.KeyEvent.VK_F;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.Stack;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class createPaint extends JPanel {

    private static final long serialVersionUID = 1L;
    private static JPanel buttonPanel;
    private static JFrame frame;
    private static ImagePanel canvas;

    private static JMenuBar menuBar = new JMenuBar();
    private static JMenu menu = new JMenu("Menu");
    private static JScrollPane scroll;
    public JTextField xfield;
    public JTextField yfield;
    private static JButton closeHelp;
    //Creates the menu items
    public JMenuItem saveButton = new JMenuItem("Save");
    public JMenuItem openButton = new JMenuItem("Open");
    public JMenuItem saveAsButton = new JMenuItem("Save As");
    public JMenuItem helpButton = new JMenuItem("Help");
    public JMenuItem closeButton = new JMenuItem("Close");
    public JMenuItem resizeButton = new JMenuItem("Resize");
    
    public JColorChooser Jcc = new JColorChooser();
    private File filepath;
    public int currentTool = 1;
    public Color newColor;
    JButton colorChooser = new JButton();
    Stack<Graphics2D> history = new Stack<>();
    public createPaint() {
        //Creates the Main Frame

        frame = new JFrame("Paint");
        canvas = new ImagePanel();

        /*Makes a mouse listener for the canvas, so that when it is Pressed, 
        the ColorChooser's Icon will update accourdingly */
        canvas.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (canvas.getTool() == 7) {
                    BufferedImage newimage = new BufferedImage(100, 100, java.awt.image.BufferedImage.TYPE_INT_RGB);
                    Graphics2D graphics = newimage.createGraphics();
                    graphics.setColor(canvas.graphics2D.getColor());
                    graphics.fillRect(0, 0, 100, 100);
                    graphics.setXORMode(Color.DARK_GRAY);
                    graphics.drawRect(0, 0, 99, 99);
                    newimage.flush();
                    ImageIcon icon = new ImageIcon(newimage);
                    colorChooser.setIcon(icon);
                }

            }
        });
        //Creates and instanciates Menubar
        makeMenuBar(menuBar);
        JButton zoomIn = new JButton("+");
        JButton zoomReset = new JButton("Reset");
        JButton zoomOut = new JButton("-");
        JButton undoButton= new JButton("Undo");
        //Creates the Tool Buttons and links them together
        ButtonGroup cbg = new ButtonGroup();
        JRadioButton lineButton = new JRadioButton("Line");
        JRadioButton drawButton = new JRadioButton("Draw");
        JRadioButton squareButton = new JRadioButton("Square");
        JRadioButton rectangleButton = new JRadioButton("Rectangle");
        JRadioButton circleButton = new JRadioButton("Circle");
        JRadioButton ovalButton = new JRadioButton("Oval");
        JRadioButton dropperTool = new JRadioButton("Dropper");
        undoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Calls the undo funcion of the Canvas
                try {
                    canvas.undo();
                } catch (IOException ex) {
                    Logger.getLogger(createPaint.class.getName()).log(Level.SEVERE, null, ex);
                }
                canvas.repaint();
            }
        });
        //Adds listener to to "Zoomin" Button that calls the respecctive funstion in the imagePane
        zoomIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.zoomIn();
                canvas.repaint();
            }
        });
        //Adds listener to to "ZoomOut" Button that calls the respecctive funstion in the imagePane
        zoomOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.zoomOut();
                canvas.repaint();
            }
        });
        //Adds listener to to "ZoomReset" Button that calls the respecctive funstion in the imagePane
        zoomReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.zoomReset();
                canvas.repaint();
            }
        });

        //Adds actionListener to each of the Buttons
        drawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setTool(2);
                canvas.repaint();
            }
        });
        lineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setTool(1);
                canvas.repaint();
            }
        });
        squareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setTool(3);
                canvas.repaint();
            }
        });
        rectangleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setTool(4);
                canvas.repaint();
            }
        });
        circleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setTool(5);
                canvas.repaint();
            }
        });
        ovalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setTool(6);
                canvas.repaint();
            }
        });
        dropperTool.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setTool(7);
                canvas.repaint();

            }
        });
        //Links the tools together so only one can be selected at a time
        cbg.add(drawButton);
        cbg.add(lineButton);
        cbg.add(squareButton);
        cbg.add(rectangleButton);
        cbg.add(circleButton);
        cbg.add(ovalButton);
        cbg.add(dropperTool);

        //Create slider that sets width
        JSlider width = new JSlider(1, 50, 10);
        width.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    canvas.setStroke((int) source.getValue());
                }
            }
        ;
        });
        width.setMajorTickSpacing(10);
        width.setPaintTicks(true);

        //Creates the colorChooser Button
        createColorchooser(colorChooser);
        colorChooser.setPreferredSize(new Dimension(100, 100));
        //Creates the the diffrent JPanels that are in the ToolBar
        JPanel toolPanel = new JPanel();
        JPanel colorPanel = new JPanel();
        JPanel shapePanel = new JPanel();
        JPanel brushPanel = new JPanel();
        JPanel zoomPanel = new JPanel();

        colorPanel.setLayout(new FlowLayout());
        toolPanel.setLayout(new BoxLayout(toolPanel, BoxLayout.PAGE_AXIS));
        brushPanel.setLayout(new BoxLayout(brushPanel, BoxLayout.PAGE_AXIS));

        shapePanel.setLayout(new GridLayout(0, 2));
        zoomPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        colorPanel.add(colorChooser);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 40;      
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 1;
        brushPanel.add(colorPanel, c);
        c.gridwidth = 1;
        c.ipady = 10;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        zoomPanel.add(zoomIn, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        zoomPanel.add(zoomReset, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 2;
        c.gridy = 0;

        zoomPanel.add(zoomOut, c);
        c.gridx = 0;
        c.gridy = 2;
        brushPanel.add(zoomPanel, c);

        shapePanel.setBorder(BorderFactory.createLineBorder(Color.black));
        brushPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        
        brushPanel.add(width);
        brushPanel.add(drawButton);
        brushPanel.add(dropperTool);
        brushPanel.add(lineButton);
        brushPanel.add(zoomPanel);
        brushPanel.add(undoButton);
        shapePanel.add(squareButton);
        shapePanel.add(rectangleButton);
        shapePanel.add(circleButton);
        shapePanel.add(ovalButton);

        toolPanel.add(brushPanel);
        toolPanel.add(shapePanel);

        JScrollPane scroll = new JScrollPane(canvas);
        //Adds Componenets to the main Frame
        frame.add(menuBar, BorderLayout.NORTH);
        frame.add(toolPanel, BorderLayout.WEST);
        frame.add(scroll, BorderLayout.CENTER);
        //Makes Frame Visable
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(1280, 700);
        frame.setVisible(true);
    }

    private void makeMenuBar(JMenuBar menuBar) {
        //creates the Help popup menu
        JPopupMenu helpPopup = new JPopupMenu("Help");
        helpPopup.setVisible(false);
        helpPopup.setSize(600, 500);
        helpPopup.setLocation(100, 100);
        JButton closePopup = new JButton("Close Help");
        closePopup.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                helpPopup.setVisible(false);
            }
        });

        JLabel helpContent = new JLabel("This is the Help Menu! It Doesn't do alot yet");
        helpPopup.add(helpContent);
        helpPopup.addSeparator();
        helpPopup.add(closePopup);
        KeyStroke keyStrokeToSave = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK);
        saveButton.setAccelerator(keyStrokeToSave);
        //Creates the code for the "Open" Button
        openButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                try {
                    canvas.openImage();
                } catch (IOException ex) {
                    Logger.getLogger(createPaint.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
        //Creates the code for the "Save" Menu Button
        saveButton.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(java.awt.event.ActionEvent e) {
                canvas.saveImage();
            }
        });

        saveAsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
              
                if (canvas.getImage() != null) {
                    
                    JFileChooser fc = new JFileChooser("/home");
               fc.setDialogTitle("Save file as JPG");
               int ret = fc.showSaveDialog(null);
               if (ret == JFileChooser.APPROVE_OPTION) {
                  File file = fc.getSelectedFile();
                        try {
                            ImageIO.write((RenderedImage) canvas.getImage(), "jpg", file);
                        } catch (IOException ex) {
                            Logger.getLogger(PaintProgram.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }}}});
            
        
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                System.exit(0);
            }
        });

        helpButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                helpPopup.setVisible(true);
            }

        });
        resizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                //Creates Popup that asks the user for the new Width and Height
                JFrame resize = new JFrame();
                resize.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                resize.setPreferredSize(new Dimension(200, 200));
                resize.setLayout(new FlowLayout());

                JPanel x = new JPanel(new BorderLayout());
                JPanel y = new JPanel(new BorderLayout());
                JLabel xText = new JLabel("Width=");
                JLabel yText = new JLabel("Height=");
                xfield = new JTextField(10);
                yfield = new JTextField(10);
                x.add(xText, BorderLayout.LINE_START);
                x.add(xfield, BorderLayout.EAST);
                y.add(yText, BorderLayout.LINE_START);
                y.add(yfield, BorderLayout.EAST);
                
                //adds the "Confim" Button that when pressed will call the canvas'
                //resize function, and will resize it to match the new sizes
                JButton computeButton = new JButton("Change Size");
                computeButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int newx = Integer.parseInt(xfield.getText());
                        int newy = Integer.parseInt(yfield.getText());
                        canvas.resizeCanvas(newx, newy);
                        resize.dispose();
                    }

                });
                resize.add(x);
                resize.add(y);
                resize.add(computeButton);
                resize.pack();
                resize.setVisible(true);
            }

        });
        //Adds KeyBoard commands for the menu
        KeyStroke menuKeys = KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK);
        menu.setMnemonic(VK_F);
        //Adds menu Buttons to the Menu
        menu.add(openButton);
        menu.add(saveButton);
        menu.add(saveAsButton);
        menu.add(helpButton);
        menu.add(resizeButton);
        menuBar.add(menu);
        menuBar.add(closeButton);


    }

    private void createColorchooser(JButton colorChooser) {
        //Makes the default Color image when the program is first opened
        Color current = Color.BLACK;
        BufferedImage image = new BufferedImage(100, 100, java.awt.image.BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();
        graphics.setColor(current);
        graphics.fillRect(0, 0, 100, 100);
        graphics.setXORMode(Color.DARK_GRAY);
        graphics.drawRect(0, 0, 99, 99);
        image.flush();
        ImageIcon icon = new ImageIcon(image);
        colorChooser.setIcon(icon);
        colorChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                //When Button is pressed, will request the user to select a color
                Color newColor = JColorChooser.showDialog(null, "Choose a color", canvas.getColor());
                Color current = newColor;
                //Makes a new Image icon for the Colorchooser, that will match the currently selected color
                
                BufferedImage image = new BufferedImage(100, 100, java.awt.image.BufferedImage.TYPE_INT_RGB);
                Graphics2D graphics = image.createGraphics();
                graphics.setColor(newColor);
                graphics.fillRect(0, 0, 100, 100);
                graphics.setXORMode(Color.DARK_GRAY);
                graphics.drawRect(0, 0, 99, 99);
                image.flush();
                ImageIcon icon = new ImageIcon(image);
                colorChooser.setIcon(icon);
                canvas.setColor(current);
            }
        });
    }

}
